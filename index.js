var MongoClient = require('mongodb').MongoClient;
const fs = require('fs');

var url = 'mongodb://127.0.0.1:27017/expresscart';
const rawTestData = fs.readFileSync('testdata2.json', 'utf-8');
const testData = JSON.parse(rawTestData).products;
MongoClient.connect(url, function(err, db) {
  if (err) throw err;
  var dbo = db.db('expresscart');
  dbo.collection('products').insertMany(testData, function(err, res) {
    if (err) throw err;
    console.log('add Succesfully' + testData.length);
    db.close();
  });
});
